# =====================================================================================================================
# === Google Cloud template variant
# =====================================================================================================================
spec:
  inputs:
    kubectl-image:
      description: The Docker image used to run Kubernetes `kubectl` commands on [GKE](https://cloud.google.com/kubernetes-engine/docs)
      default: gcr.io/google.com/cloudsdktool/cloud-sdk:latest  
    gcp-oidc-aud:
      description: The `aud` claim for the JWT token
      default: $CI_SERVER_URL
    gcp-oidc-account:
      description: Default Service Account to which impersonate with OpenID Connect authentication
      default: ''
    gcp-oidc-provider:
      description: Default Workload Identity Provider associated with GitLab to [authenticate with OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/google_cloud/)
      default: ''
    gcp-review-oidc-account:
      description: Service Account to which impersonate with OpenID Connect authentication on `review` environment
      default: ''
    gcp-review-oidc-provider:
      description: Workload Identity Provider associated with GitLab to [authenticate with OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/google_cloud/) on `review` environment
      default: ''
    gcp-integ-oidc-account:
      description: Service Account to which impersonate with OpenID Connect authentication on `integration` environment
      default: ''
    gcp-integ-oidc-provider:
      description: Workload Identity Provider associated with GitLab to [authenticate with OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/google_cloud/) on `integration` environment
      default: ''
    gcp-staging-oidc-account:
      description: Service Account to which impersonate with OpenID Connect authentication on `staging` environment
      default: ''
    gcp-staging-oidc-provider:
      description: Workload Identity Provider associated with GitLab to [authenticate with OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/google_cloud/) on `staging` environment
      default: ''
    gcp-prod-oidc-account:
      description: Service Account to which impersonate with OpenID Connect authentication on `production` environment
      default: ''
    gcp-prod-oidc-provider:
      description: Workload Identity Provider associated with GitLab to [authenticate with OpenID Connect](https://docs.gitlab.com/ee/ci/cloud_services/google_cloud/) on `production` environment
      default: ''
---
variables:
  # variabilized gcp-auth-provider image
  GCP_OIDC_AUD: $[[ inputs.gcp-oidc-aud ]]
  GCP_OIDC_ACCOUNT: $[[ inputs.gcp-oidc-account ]]
  GCP_OIDC_PROVIDER: $[[ inputs.gcp-oidc-provider ]]
  GCP_REVIEW_OIDC_ACCOUNT: $[[ inputs.gcp-review-oidc-account ]]
  GCP_REVIEW_OIDC_PROVIDER: $[[ inputs.gcp-review-oidc-provider ]]
  GCP_INTEG_OIDC_ACCOUNT: $[[ inputs.gcp-integ-oidc-account ]]
  GCP_INTEG_OIDC_PROVIDER: $[[ inputs.gcp-integ-oidc-provider ]]
  GCP_STAGING_OIDC_ACCOUNT: $[[ inputs.gcp-staging-oidc-account ]]
  GCP_STAGING_OIDC_PROVIDER: $[[ inputs.gcp-staging-oidc-provider ]]
  GCP_PROD_OIDC_ACCOUNT: $[[ inputs.gcp-prod-oidc-account ]]
  GCP_PROD_OIDC_PROVIDER: $[[ inputs.gcp-prod-oidc-provider ]]
  
  K8S_KUBECTL_IMAGE: $[[ inputs.kubectl-image ]]

.gcp-provider-auth:
  before_script:
    - echo "Installing GCP authentication with env GOOGLE_APPLICATION_CREDENTIALS file"
    - echo $GCP_JWT > "$CI_BUILDS_DIR/.auth_token.jwt"
    - |-
      if [[ "$ENV_TYPE" ]]
      then
        case "$ENV_TYPE" in
        review*)
          env_prefix=REVIEW;;
        integ*)
          env_prefix=INTEG;;
        staging*)
          env_prefix=STAGING;;
        prod*)
          env_prefix=PROD;;
        *)
          ;;
        esac
        env_oidc_provider=$(eval echo "\$GCP_${env_prefix}_OIDC_PROVIDER")
        env_oidc_account=$(eval echo "\$GCP_${env_prefix}_OIDC_ACCOUNT")
      fi
      oidc_provider="${env_oidc_provider:-$GCP_OIDC_PROVIDER}"
      oidc_account="${env_oidc_account:-$GCP_OIDC_ACCOUNT}"
    - |-
      cat << EOF > "$CI_BUILDS_DIR/google_application_credentials.json"
      {
        "type": "external_account",
        "audience": "//iam.googleapis.com/${oidc_provider}",
        "subject_token_type": "urn:ietf:params:oauth:token-type:jwt",
        "token_url": "https://sts.googleapis.com/v1/token",
        "credential_source": {
          "file": "$CI_BUILDS_DIR/.auth_token.jwt"
        },
        "service_account_impersonation_url": "https://iamcredentials.googleapis.com/v1/projects/-/serviceAccounts/${oidc_account}:generateAccessToken"
      }
      EOF
    - export GOOGLE_APPLICATION_CREDENTIALS="$CI_BUILDS_DIR/google_application_credentials.json"

.k8s-deploy:
  id_tokens:
    GCP_JWT:
      aud: "$GCP_OIDC_AUD"
  before_script:
    - !reference [.k8s-scripts]
    - !reference [.gcp-provider-auth, before_script]
    - install_ca_certs "${CUSTOM_CA_CERTS:-$DEFAULT_CA_CERTS}"
    - k8s_login

.k8s-cleanup:
  id_tokens:
    GCP_JWT:
      aud: "$GCP_OIDC_AUD"
  before_script:
    - !reference [.k8s-scripts]
    - !reference [.gcp-provider-auth, before_script]    
    - install_ca_certs "${CUSTOM_CA_CERTS:-$DEFAULT_CA_CERTS}"
    - k8s_login